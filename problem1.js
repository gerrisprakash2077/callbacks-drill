
 const fs=require('fs')
 const path=require('path')


function createAndDeleteFiles(data){
    fs.mkdir(path.resolve(__dirname,'./randomDirectory'),(err)=>{
        if(err){
            console.error(err);
        }
        else{
            data.forEach((element,index) => {
                fs.writeFile(path.resolve(__dirname,element[1]),element[0],(err)=>{
                    if(err){
                        console.error(err);
                    }
                    else{
                        console.log("json created");
                        if(index==data.length-1){
                            data.forEach(element=>{
                                fs.unlink(path.resolve(__dirname,element[1]),(err)=>{
                                    if(err){
                                        console.error(err);
                                    }
                                    else{
                                        console.log('json Deleted');
                                    }
                                })
                            })
                        }
                    }
                })
            });
            
        }
    });
}

module.exports=createAndDeleteFiles;
