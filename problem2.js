/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs= require("fs");
const path=require('path');

function operation(){
    fs.readFile(path.resolve(__dirname,'./lipsum.txt'),'utf8',(err,lipsumData)=>{
        if(err){
            console.error(err);
        }
        else{
            console.log('Read lipsum');
            lipsumData=lipsumData.toUpperCase();
            fs.writeFile('./lipsumuppercaseFile.txt',lipsumData,'utf8',(err)=>{
                if(err){
                    console.error(err);
                }else{
                    console.log('written to upperCaseFile');
                    fs.writeFile('./filename.txt','./lipsumuppercaseFile.txt','utf8',(err)=>{
                        if(err){
                            console.error(err);
                        }else{
                            fs.readFile('./lipsumuppercaseFile.txt','utf8',(err,upperCaseData)=>{
                                if(err){
                                    console.error(err);
                                }
                                else{
                                    console.log('read uppercase data');
                                    upperCaseData=upperCaseData.toLowerCase().split('. ').join('\n').split('\n').filter((ele)=>{
                                        return ele!='';
                                    }).join('\n');
                                    console.log('converted to sentences');
                                    fs.writeFile('./lipsumlowercaseSentencesfile.txt',upperCaseData,'utf8',(err)=>{
                                        if(err){
                                            console.error(err);
                                        }else{
                                            console.log('written to lipsumlowercaseSentences file');
                                            fs.appendFile('./filename.txt',','+'./lipsumlowercaseSentencesfile.txt','utf8',(err)=>{
                                                if(err){
                                                    console.error(err);
                                                }
                                                else{
                                                    fs.readFile('./lipsumlowercaseSentencesfile.txt','utf8',(err,sentData)=>{
                                                        if(err){
                                                            console.error(err);
                                                        }
                                                        else{
                                                            sentData=sentData.split('\n').sort().join('.\n');
                                                            fs.writeFile('./lipsumSortedFile.txt',sentData,'utf8',(err)=>{
                                                                if(err){
                                                                    console.error(err);
                                                                }else{
                                                                    console.log('lipsum sorted file created');
                                                                    fs.appendFile('./filename.txt',','+'./lipsumSortedFile.txt','utf8',(err)=>{
                                                                        if(err){
                                                                            console.error(err);
                                                                        }else{
                                                                            fs.readFile('filename.txt','utf8',(err,fileNameData)=>{
                                                                                if(err){
                                                                                    console.error(err);
                                                                                }else{
                                                                                    fileNameData.split(',').forEach((seperateFileNames)=>{
                                                                                        fs.unlink(seperateFileNames,(err)=>{
                                                                                            if(err){
                                                                                                console.error(err);
                                                                                            }else{
                                                                                                console.log('deleted');
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
    
}
module.exports=operation;