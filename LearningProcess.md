# Learning Process

## 1. What is the Feynman Technique? Paraphrase the video in your own words.


Feyman technique is used to check the deep understanding of a particular concept.


The video explains the Feymans technique to learn a concept in depth. The the process are as follows.

* write the concept name in a piece of paper.
* Explain the concept in simple language.
* Identify the problem area then check back the source to review.
* Pinpoint complicated areas and simplify yourself.


## 2. What are the different ways to implement this technique in your learning process?
* Question myself about the concept which I was learning.
* Explaining the concept to myself in my mind and write down the explanation in a piece of paper.

## 3. Paraphrase the video in detail in your own words.
The video is about how to learn
* Some biography of Barbara Oakley
* How brain works at focused and relaxed mode
* How we can use the advantages of both the modes to learn faster.
## 4. What are some of the steps that you can take to improve your learning process?

If I was about to learn a new concept then I have to do it in a relaxed way. 
## 5. Your key takeaways from the video? Paraphrase your understanding.

## 6. What are some of the steps that you can while approaching a new topic?